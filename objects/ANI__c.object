<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>Accept</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Follow</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <type>Default</type>
    </actionOverrides>
    <allowInChatterGroups>false</allowInChatterGroups>
    <compactLayoutAssignment>SYSTEM</compactLayoutAssignment>
    <deploymentStatus>Deployed</deploymentStatus>
    <description>ACC - Custom object to store ANI number</description>
    <enableActivities>false</enableActivities>
    <enableBulkApi>true</enableBulkApi>
    <enableEnhancedLookup>false</enableEnhancedLookup>
    <enableFeeds>false</enableFeeds>
    <enableHistory>false</enableHistory>
    <enableReports>false</enableReports>
    <enableSearch>true</enableSearch>
    <enableSharing>true</enableSharing>
    <enableStreamingApi>true</enableStreamingApi>
    <externalSharingModel>ReadWrite</externalSharingModel>
    <fields>
        <fullName>ANINumber__c</fullName>
        <externalId>false</externalId>
        <label>ANI Number</label>
        <required>true</required>
        <trackTrending>false</trackTrending>
        <type>Phone</type>
    </fields>
    <fields>
        <fullName>Account__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <externalId>false</externalId>
        <label>Account</label>
        <referenceTo>Account</referenceTo>
        <relationshipLabel>ANI (Additional Phone Numbers)</relationshipLabel>
        <relationshipName>ANIs</relationshipName>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Contact__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <externalId>false</externalId>
        <label>Contact</label>
        <referenceTo>Contact</referenceTo>
        <relationshipLabel>ANI (Additional Phone Numbers)</relationshipLabel>
        <relationshipName>ANIs</relationshipName>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>TECH_Account__c</fullName>
        <description>Field displayed in CTI Softphone on Single match scenario (when single ANI Number (ANI object) match caller phone Phone Number)</description>
        <externalId>false</externalId>
        <formula>Account__r.Name</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Account</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>TECH_ContactName__c</fullName>
        <description>Field displayed in CTI Softphone on Single match scenario (when single ANI Number (ANI object) match caller phone Phone Number)</description>
        <externalId>false</externalId>
        <formula>Contact__r.FirstName &amp; &quot; &quot; &amp; Contact__r.LastName</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Contact</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>TECH_ContactSupportLevel__c</fullName>
        <description>Field displayed in CTI Softphone on Single match scenario (when single ANI Number (ANI object) match caller phone Phone Number)</description>
        <externalId>false</externalId>
        <formula>TEXT(Contact__r.ContactSupportLevel__c)</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Contact Support Level</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>TECH_CorrespondanceLanguage__c</fullName>
        <description>Field displayed in CTI Softphone on Single match scenario (when single ANI Number (ANI object) match caller phone Phone Number)</description>
        <externalId>false</externalId>
        <formula>TEXT(Contact__r.CorrespLang__c)</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Correspondance Language</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>TECH_ServiceContractType__c</fullName>
        <description>Field displayed in CTI Softphone on Single match scenario (when single ANI Number (ANI object) match caller phone Phone Number)</description>
        <externalId>false</externalId>
        <formula>TEXT(Contact__r.ServiceContractType__c)</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Service Contract Type</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <label>ANI</label>
    <listViews>
        <fullName>All</fullName>
        <columns>NAME</columns>
        <columns>Account__c</columns>
        <columns>Contact__c</columns>
        <columns>ANINumber__c</columns>
        <columns>CREATED_DATE</columns>
        <columns>CREATEDBY_USER</columns>
        <filterScope>Everything</filterScope>
        <label>All</label>
        <language>en_US</language>
    </listViews>
    <nameField>
        <displayFormat>ANI-{00000}</displayFormat>
        <label>ANI</label>
        <type>AutoNumber</type>
    </nameField>
    <pluralLabel>ANIs</pluralLabel>
    <searchLayouts>
        <lookupDialogsAdditionalFields>Contact__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>Account__c</lookupDialogsAdditionalFields>
        <searchResultsAdditionalFields>Contact__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>Account__c</searchResultsAdditionalFields>
    </searchLayouts>
    <sharingModel>ReadWrite</sharingModel>
    <startsWith>Vowel</startsWith>
    <validationRules>
        <fullName>ANI_VR01_MustHaveAccountOrContact</fullName>
        <active>true</active>
        <description>An ANI number must always be related to a contact or an account</description>
        <errorConditionFormula>AND(NOT($User.BypassVR__c),
OR(AND(NOT(ISBLANK( Account__c )), NOT(ISBLANK( Contact__c ))),
ISBLANK( Account__c ) &amp;&amp;  ISBLANK( Contact__c ))
)</errorConditionFormula>
        <errorMessage>Please select a Contact or an Account.</errorMessage>
    </validationRules>
    <validationRules>
        <fullName>ANI_VR02_AccountControl</fullName>
        <active>true</active>
        <description>July 2011: Ensures that the Account has not been marked for deletion</description>
        <errorConditionFormula>NOT($User.BypassVR__c) &amp;&amp; (ISNEW() || ISCHANGED(  Account__c  )) &amp;&amp;( Account__r.ToBeDeleted__c = TRUE )</errorConditionFormula>
        <errorDisplayField>Account__c</errorDisplayField>
        <errorMessage>This Account has been marked for deletion, please choose another one.</errorMessage>
    </validationRules>
</CustomObject>
